package com.tutorial.dao;

import com.tutorial.domain.Author;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
public class authorRepository {
    @PersistenceContext
    EntityManager entityManager;

    public void save(Author author){
        if(StringUtils.isEmpty(author.getAuthorId())){
            entityManager.persist(author);
        }
    }

    public Author getAuthor(int authorId){
        TypedQuery<Author> query = entityManager.createQuery("from Authors where authorId = :authorId", Author.class);
        query.setParameter("authorId", authorId);
        return query.getSingleResult();
    }
}
